//Peng Liang,cs201-202201,Mar. 7, 2022,CTAStation,sec.#03
package labs.lab5;

public class CTAStation extends GeoLocation {
	
	// create variables
    private String name;
    private String location;
    private boolean wheelchair;
    private boolean open;
    
    public CTAStation() {
        super();
        this.name = "unnamed";
        this.location = "";
        this.wheelchair = false;
        this.open = false;
    }
    
    public CTAStation(String name, double lat, double lng, String location, boolean wheelchair, boolean open ) {
        super(lat, lng);
        this.name = name;
        this.location = location;
        this.wheelchair = wheelchair;
        this.open = open;
    }
    
    public String getName() {
        return this.name;
    }

    public String getLocation() {
        return this.location;
    }

    public boolean hasWheelchair() {
        return this.wheelchair;
    }

    public boolean isOpen() {
        return this.open;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWheelchair(boolean wheelchair) {
        this.wheelchair = wheelchair;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }
    
    public String toString() {
    	return " name: " + this.name + " geolocation: " + super.toString() + " location: " + this.location + " wheelchair?: " + this.wheelchair + " open?: " + this.isOpen();
    }
    
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        if (!(obj instanceof CTAStation)) {
            return false;
        }
        CTAStation c = (CTAStation) obj;
        return this.name.equals(c.getName()) && 
        		this.location.equals(c.getLocation()) && 
        		this.wheelchair == c.hasWheelchair() && 
        		this.open == c.isOpen();
    }
}
