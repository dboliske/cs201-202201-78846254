//Peng Liang,cs201-202201,Mar. 7, 2022,CTAStopApp,sec.#03
package labs.lab5;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CTAStopApp {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		CTAStation[] stations = readFile("src/labs/lab5/CTAStops.csv");
		menu(stations);
        input.close();
        System.out.println("Goodbye!");
	}
	
	public static CTAStation[] readFile(String filename){
        CTAStation[] stations = new CTAStation[10];
		int count = 0;
		
		try {
			File f = new File(filename);
			Scanner input = new Scanner(f);
			input.nextLine();
			while (input.hasNextLine()) {
				try {
					String line = input.nextLine();
					String[] values = line.split(",");
                    
					CTAStation c = new CTAStation(values[0],Double.parseDouble(values[1]),Double.parseDouble(values[2]),values[3], Boolean.parseBoolean(values[4]),Boolean.parseBoolean(values[5]));
					
					if (stations.length == count) {
						stations =  resize(stations, stations.length*2);
					}
					
					stations[count] = c;
					count++;
				} catch (Exception e) {
					System.out.println("The data format of line " + (count+2) +" in csv file is incorrect");
				}
			}
			input.close();
		} catch (FileNotFoundException fnf) {
			System.out.println("File Not Found!");
            return null;
		} catch(Exception e) {
			System.out.println("Error occurred reading in file.");
		}
		
		stations = resize(stations, count);
		return stations;
	}
	
	public static CTAStation[] resize(CTAStation[] stations, int size) {
		CTAStation[] temp = new CTAStation[size];
		int limit = stations.length > size ? size : stations.length;
		for (int i=0; i<limit; i++) {
			temp[i] = stations[i];
		}
		
		return temp;
	}
	
    public static void menu(CTAStation[] stations){
    	Scanner input = new Scanner(System.in);
    	boolean done = false;
		
		do {
			System.out.println("1. Display Station Names");
			System.out.println("2. Display Stations with/without Wheelchair access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			System.out.print("Please choose: ");
			
			String choice = input.nextLine();
			
			switch (choice) {
				case "1": 
                    displayStationNames(stations);
					break;
				case "2": 
                    displayByWheelchair(input,stations);
					break;
				case "3": 
                    displayNearest(input,stations);
					break;
				case "4": 
					done = true;
					break;
				default:
					System.out.println("Sorry, this option is not available.");
			}
			
		} while (!done);
		input.close();
	
    }
    
    public static void displayStationNames(CTAStation[] stations){
        for(int i = 0; i<stations.length; i++){
            System.out.println(stations[i].getName());
        }
    }
    
    public static void displayByWheelchair(Scanner input,CTAStation[] stations){
    	boolean done = false;
    	do {
            System.out.print("Plaese enter an 'A' for stations with wheelchair access but 'B' for stations without wheelchair access: ");
            String in = input.nextLine();
            boolean hasStationWithWheelchair = false;
            boolean hasStationWithoutWheelchair = false;
            switch (in) {
                case "A":
                    for (int i = 0; i < stations.length; i++) {
                        if (stations[i].hasWheelchair()) {
                            System.out.println(stations[i]);
                            hasStationWithWheelchair = true;
                        }
                    }
                    if (!hasStationWithWheelchair) {
                        System.out.println("Not Found!");
                    }
                    done = true;
                    break;
                case "B":
                    for (int i = 0; i < stations.length; i++) {
                        if (!stations[i].hasWheelchair()) {
                            System.out.println(stations[i]);
                            hasStationWithoutWheelchair = true;
                        }
                    }
                    if (!hasStationWithoutWheelchair) {
                        System.out.println("Not Found!");
                    }
                    done = true;
                    break;
                default:
                    System.out.println("Sorry, this option is not available.");
            }
        } while (!done);
    }
    
    public static void displayNearest(Scanner input,CTAStation[] stations){
    	double[] distances = new double[stations.length];
    	
    	try {
    	      //Prompt the user for the latitude and longitude
              System.out.print("Enter a latitude: ");
              double lat = Double.parseDouble(input.nextLine());
              System.out.print("Enter a longitude: ");
              double lng = Double.parseDouble(input.nextLine());

              for (int i = 0; i < distances.length; i++) {
                    distances[i] = stations[i].calcDistance(lat, lng);
              }
        } catch (Exception e) {
        System.out.println("Invalid input!");
        }

        double nearestDistance = distances[0];
        int count = 0;
    for (int i = 1; i < distances.length; i++) {
        if (distances[i] < nearestDistance) {
        	nearestDistance = distances[i];
            count = i;
        }
    }
        System.out.println("The nearest station is " + stations[count].toString());
    }
}
