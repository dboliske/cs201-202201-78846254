//Peng Liang,cs201-202201,Mar. 7, 2022,GeoLocation,sec.#03
package labs.lab5;

public class GeoLocation {

	private double lat;
	private double lng;
	
	GeoLocation(){         // default constructor
		lat = 21.545645;
		lng = 4.6455645;
	}
	
	public GeoLocation(double lat, double lng) {    // non-default constructor
		this.lat = 21.545645;
		setLat(lat);
		this.lng = 4.6455645;
		setLng(lng);
	}
	
	public double getLat() {    // accessor methods
		return lat;
	}
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) {    // mutator methods
		if (validLat(lat)) {
		       this.lat = lat;
		}
	}
	public void setLng(double lng) {
		if (validLng(lat)) {
		       this.lng = lng;
		}
	}
	
	// toString method
	public String toString() {
		return "(" + lat + ", " + lng + ")";
	}
	
	// return true if the latitude is between and (-90,90)
	public boolean validLat(double lat) {
		if (this.lat >= -90 && this.lat <= 90) {
			return true;
		}
		return false;
	}
	
	// return true if the longitude is between and (-180,180)
	public boolean validLng(double lng) {
		if (this.lng >= -180 && this.lng <= 180) {
			return true;
		}
		return false;
	}
	
	// equals
	public boolean equals(GeoLocation g) {
		if (this.lat != g.getLat()) {
			return false;
		} else if (this.lng != g.getLng()) {
			return false;
		}
		return true;
	}	
	
	//	calculate the distance
	public double calcDistance(GeoLocation g) {
		return Math.sqrt(Math.pow(this.getLat() - g.getLat(), 2) + Math.pow(this.getLng() - g.getLng(), 2));
	}

	public double calcDistance(double lat, double lng) {
		return Math.sqrt(Math.pow(this.getLat() - lat, 2) + Math.pow(this.getLng() - lng, 2));
	}
}
