//Peng Liang,cs201-202201,Apr. 4, 2022,Exercise4,sec.#03
package labs.lab7;

import java.util.Scanner;

public class Exercise4 {
	
    public static int search(String[] array, String value) {
        return binarySearch(array, 0, array.length-1, value);
    }

    public static int binarySearch(String[] array, int start, int end, String value) {
        if (start > end) {
            return -1;
        }

        int mid = (start + end)/2;
        if (value.equalsIgnoreCase(array[mid])) {
            return mid;
        } else if (value.compareToIgnoreCase(array[mid]) > 0) {
            return binarySearch(array, mid+1, start, value);
        } else {
            return binarySearch(array, end, mid-1, value);
        }
    }
    
    public static void main(String[] args) {
        String[] array = {"c", "html", "java", "python", "ruby", "scala"};

        Scanner input = new Scanner(System.in);
        System.out.print("Search term: ");
        String value = input.nextLine();

        int index = search(array, value);

        if (index == -1) {
            System.out.println(value + " not found.");
        } else {
            System.out.println(value + " found at index " + index + ".");
        }

        input.close();
    }
}
