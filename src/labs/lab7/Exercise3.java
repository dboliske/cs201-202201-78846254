//Peng Liang,cs201-202201,Apr. 4, 2022,Exercise3,sec.#03
package labs.lab7;

public class Exercise3 {

	public static double[] selectionSort(double[] array) {
        for (int i = 0; i <array.length - 1; i++) {
            int min = i;
            
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[min]) {
                    min = j;
                }
            }

            if (min != i) {
                double temp = array[min];
                array[min] = array[i];
                array[i] = temp;
            }

        }
        return array;
    }
    public static void main(String[] args) {
        double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282};
        array = selectionSort(array);
        for (double d : array) {
            System.out.print(d + " ");
        }
    }

}
