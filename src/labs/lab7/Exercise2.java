//Peng Liang,cs201-202201,Apr. 4, 2022,Exercise2,sec.#03
package labs.lab7;

public class Exercise2 {

	public static String[] insertionSort(String[] array) {
        for (int i = 1; i < array.length; i++) {
            for(int j = i; j > 0 && array[j].compareTo(array[j-1]) < 0; j--) {
                String temp = array[j];
                array[j] = array[j-1];
                array[j-1] = temp;
            }
        }
        return array;
    }


    public static void main(String[] args) {
        String[] array = {"cat", "fat", "dog", "apple", "bat", "egg"};
        array = insertionSort(array);
        for (String word : array) {
            System.out.print(word + " ");
        }
    }

}
