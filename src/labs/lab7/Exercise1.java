//Peng Liang,cs201-202201,Apr. 4, 2022,Exercise1,sec.#03
package labs.lab7;

public class Exercise1 {

	public static int[] bubbleSort(int[] array) {
        boolean done;
        do {
            done = true;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i+1]) {
                    int temp = array[i];
                    array[i] = array[i+1];
                    array[i+1] = temp;
                    done = false;
                }
            }
        } while (!done);

        return array;
    }

    public static void main(String[] args) {
        int[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9};
        array = bubbleSort(array);
        for (int num:array) {
            System.out.print(num + " ");
        }
    }

}
