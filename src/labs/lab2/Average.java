//Peng Liang,cs201-202201,Feb. 6, 2022,Average
package labs.lab2;

import java.util.Scanner;

public class Average {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		for(int i = 1; i <= 1; i++) {
		    double sum = 0, count = 0;

		    while(true) {
		        System.out.printf("Enter your grade (input '-1' to finish): ");
		        int grade = Integer.parseInt(input.nextLine());
		        if(grade == -1) break;                  // finished
		        sum = sum + grade;
		        count++;
		    }

		    System.out.printf("Your average score is: " + sum / count);
		    input.close();
	    }

	}

}
