package labs.lab2;

import java.util.Scanner;

public class Menu {

	public static void main(String[] args) {
	Scanner input = new Scanner(System.in);
	boolean done = false;
	do {
		System.out.println("****Options****");
        System.out.println("1.Say Hello");
        System.out.println("2.Addition");
        System.out.println("3.Multiplication");
        System.out.println("4.Exit");
        System.out.println("Choices:");
        int choice = Integer.parseInt(input.nextLine());

        	switch(choice) {
        	case 1:
        		System.out.println("Hello!");
        		System.out.println("");
        		break;
        	case 2:
        		System.out.print("X=");
        		double x = Double.parseDouble(input.nextLine());
        		System.out.print("Y=");
        		double y = Double.parseDouble(input.nextLine());
        		System.out.println("X+Y="+(x+y));
        		System.out.println("");
        		break;
        	case 3:
        		System.out.print("A=");
        		double a = Double.parseDouble(input.nextLine());
        		System.out.print("B=");
        		double b = Double.parseDouble(input.nextLine());
        		System.out.println("A*B="+(a*b));
        		System.out.println("");
        		break;
        	case 4:
        		done = true;
        		System.out.println("");
        		break;
        	default:
				System.out.println("No option!");
				System.out.println("");
			    break;
        	}	

	}
        while(!done);
		input.close();
		System.out.println("Goodbye!");
        }

}
