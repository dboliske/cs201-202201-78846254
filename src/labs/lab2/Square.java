//Peng Liang,cs201-202201,Feb. 6, 2022,Square
package labs.lab2;

import java.util.Scanner;

public class Square {

	public static void main(String[] args) {
		
		//Create a new scanner
		Scanner input = new Scanner(System.in);
		
		//prompt the user for the size
		System.out.print("Please enter the size of square: ");
		int size = Integer.parseInt(input.nextLine());
		
		for (int i=0; i<size; i++) {//outer loop 
			for (int j=0; j<size; j++) {//inner loop
				System.out.print("* ");//print out the * at row side
			}
			System.out.println();//nextline
		}
		
		input.close();

	}

}
