//Peng Liang,cs201-202201,Feb. 20, 2022,PotionClient,sec.#03
package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		// default construction
		Potion p1 = new Potion();
		
		// non-default construction
		Potion p2 = new Potion("Flynn", 6.54);
		
		// display
		System.out.println(p1.toString());
		System.out.println(p2.toString());
	}
}
