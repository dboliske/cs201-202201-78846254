//Peng Liang,cs201-202201,Feb. 20, 2022,Potion,sec.#03
package labs.lab4;

public class Potion {

	private String name;
	private double strength;
	
	public Potion() {    // default constructor
		this.name = "peng";
		this.strength = 1.17;
	}
	
	public Potion(String name, double strength) {    // non-default constructor
		this.name = name;
		this.strength = 1.17;
		setStrength(strength);
	}
	
	public String getName() {    // accessor for name
		return name;
	}
	
	public double getStrength() {    // accessor for strength
		return strength;
	}
	
	// mutator
	public void setName(String name) {
		this.name = name;
	}
	public void setStrength(double strength) {
		if(validStrength(strength)) {	// use validStrength to validate strength
			this.strength = strength;
		}	
	}
	
	// to String method
	public String toString() {
		return name + ": " + strength;
	}
	
	// validate the strength
	public boolean validStrength(double strength) {
		return strength >= 0 && strength <= 10;
	}
	
	// compare this instance to another
	public boolean equals(Potion p) {
		return name.equals(p.name) && strength == p.strength;
	}

}
