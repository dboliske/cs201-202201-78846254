//Peng Liang,cs201-202201,Feb. 20, 2022,PhoneNumberClient,sec.#03
package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		PhoneNumber pn1 = new PhoneNumber();						// use the default constructor
		PhoneNumber pn2 = new PhoneNumber("9", "999", "5248756");	// use the non-default constructor
		
	    // Display the values of each object by calling the method
		System.out.println(pn1.toString());	
		System.out.println(pn2.toString());	

	}

}
