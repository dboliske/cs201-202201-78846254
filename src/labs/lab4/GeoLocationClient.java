//Peng Liang,cs201-202201,Feb. 20, 2022,GeoLocation,sec.#03
package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		GeoLocation g1 = new GeoLocation();
		GeoLocation g2 = new GeoLocation(88, 121);
		System.out.println("value of default constructor is : " + g1.toString());
		System.out.println("value of non-default constructor is : " + g2.toString());

	}

}
