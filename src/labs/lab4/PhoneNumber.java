//Peng Liang,cs201-202201,Feb. 20, 2022,PhoneNumber,sec.#03
package labs.lab4;

public class PhoneNumber {

	// Create three instance variables
	private String countryCode;
	private String areaCode;
	private String number;

	// default constructor
	public PhoneNumber() {
		this.countryCode = "0";
		this.areaCode = "123";
		this.number = "1234567";
	}
	
	// non-default constructor
	public PhoneNumber(String cCode, String aCode, String number) {
		this.countryCode = cCode;
		this.areaCode = "123";
		setAreaCode(aCode);
		this.number = "1234567";
		setNumber(number);
	}
	
	// accessor methods
	public String getCountryCode() {
		return countryCode;
	}
	public String getAreaCode() {
		return areaCode;
	}
	public String getNumber() {
		return number;
	}
	
	// mutator methods
	public void setCountryCode(String cCode) {
		this.countryCode = cCode;
	}
	public void setAreaCode(String aCode) {
		if (validAreaCode(aCode)) {		// use validAreaCode method to validate the input aCode, not set if invalid.
			this.areaCode = aCode;
		}
	}
	public void setNumber(String number) { 
		if (validNumber(number)) {		// use validNumber method to validate the input number, not set if invalid.
			this.number = number;
		}
		
	}
	
	// toString method
	public String toString() {
		return "+" + countryCode +" (" + areaCode + ") " + number ;
	}
	
	public boolean validAreaCode(String aCode) {
		return aCode.length() == 3;
	}
	
	public boolean validNumber(String number) {
		return number.length() == 7;
	}
	
	// PhoneNumber equals
	public boolean equals(PhoneNumber pn) {
		return countryCode.equals(pn.countryCode) && areaCode.equals(pn.areaCode) && number.equals(pn.number);
	}

}
