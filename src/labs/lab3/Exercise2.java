//Peng Liang,cs201-202201,Feb. 13, 2022,Exercise2
package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Exercise2 {

  public static void main(String[] args) {
    Scanner input = new Scanner(System.in); //create scanner for user input

    double[] array = new double[1]; 
    boolean run = true;
    int count = 0; 

    while  (run) {
      System.out.println("Enter a number or 'Done': "); //ask for input
      String NorD = input.nextLine();
      
      switch (NorD) { 
      	case "Done":      	// if the user enters "Done", the loop will stop.
      		run = false;
      		break;
      	
      	default:
      		if (count >= array.length) {
      	        double[] temp_array = new double[array.length + 1];
      	        for (int i = 0; i < array.length; i++) {
      	        	temp_array[i] = array[i];
      	        }

      	        array = temp_array;
				temp_array = null;
				
      	        array[count] = Double.parseDouble(NorD);
      	        count++;
      		} else  {
      	        array[count] = Double.parseDouble(NorD);
      	        count++;
      		}	
      }
    }

    // print the number list
    System.out.println("Number list: ");
    for (int i = 0; i < array.length; i++) {
    	System.out.println("(" + (i+1) + "):  " + (array[i]));
    }
    
    // asking for the file name
    System.out.println("Please type a file name to save: ");
    String filename = input.nextLine();
    
    // save the values to the file
    try {
    	FileWriter f = new FileWriter("./src/labs/lab3/" + filename + ".txt");  //open a new file
    	
    	for (int i = 0; i < array.length; i++) {
    		f.write(array[i] + "\n");
    	}
    	
    	// save and close.
    	f.flush();
    	f.close();
    	
    } catch (IOException e) {
    	System.out.println(e.getMessage());
    }
    
    System.out.println("Saved.");

    // close the scanner
    input.close();
    
  }
}