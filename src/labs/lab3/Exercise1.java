//Peng Liang,cs201-202201,Feb. 13, 2022,Exercise1
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class Exercise1 {

	public static void main(String[] args) throws IOException {
		File f = new File("src/labs/lab3/grades.csv");// create file object
		Scanner input = new Scanner(f);
		
		int[] grades = new int[14]; // create array
		int count = 0;
		double total = 0;
		while (input.hasNextLine()) {
			String line = input.nextLine();
			String[] values = line.split(",");
			grades[count] = Integer.parseInt(values[1]); // reads the numbers
			count++;
		}
		
		input.close();
		
		total = 0;
		for (int i=0; i<grades.length; i++) {
			total = total + grades[i]; 		// Calculate the sum of grades
		}
		
		System.out.println("Average: " + (total/grades.length));

	}

}
