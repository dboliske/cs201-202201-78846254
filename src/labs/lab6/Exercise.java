//Peng Liang,cs201-202201,Mar. 21, 2022,Exercise,sec.#03
package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class Exercise {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in); // create a scanner
        ArrayList<String> customers = new ArrayList<String>();
        
        String[] menu = {"Add customer to queue", "Help customer", "Exit"};
        boolean done = false;
        do {
            for (int i = 0; i < menu.length; i++) {
                System.out.println((i + 1) + ". " + menu[i]);
            }
            System.out.print("Your choice: ");
            String choice = input.nextLine();
            switch (choice) {
                case "1":
                    addCustomer(customers, input);
                    break;
                case "2":
                    helpCustomer(customers);
                    break;
                case "3":
                    done = true;
                    break;
                default:
                    System.out.println("No option!");
                    System.out.println();
            }

            } while (!done);
        
        input.close();
        System.out.println("Goodbye!");
	}
	
	// add the customer to Queue
	public static void addCustomer(ArrayList<String> customers, Scanner input) {
        System.out.print("Add customers: ");
        customers.add(input.nextLine());
       
        // returns the customers position in the queue
        System.out.println("Customer position: " + (customers.size()) );
        System.out.println();
    }
	
	// help customer
	public static void helpCustomer(ArrayList<String> customers) {
        if (customers.size() == 0) {
            System.out.println("No customer to help!");
            System.out.println();
        } else {
            String helped =  customers.remove(0);
            System.out.println(helped + " is removed.");
            System.out.println();
        }
    }

}
