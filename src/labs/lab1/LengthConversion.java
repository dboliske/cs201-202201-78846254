//Peng Liang,cs201-202201,Feb. 6, 2022,LengthConversion
package labs.lab1;

import java.util.Scanner;

public class LengthConversion {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		//Prompt the user for inches
		System.out.print("Enter a length in inches: ");
		double inches = Double.parseDouble(input.nextLine()); 
		
		//Convert inches to centimeters and display the result to the console
		System.out.println("Converted to centimeters:" + (inches*2.54));
		
		input.close(); // Close scanner
		
		//   input            result
		//   0                0.0
		//   56               142.24
		//   47               119.38
		//   564987           1435066.98
		//   45.1894          114.781076

	}

}
