//Peng Liang,cs201-202201,Feb. 6, 2022,Box
package labs.lab1;

import java.util.Scanner;

public class Box {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user for the length in inches of a box
		System.out.print("length in inches: ");
		double x = Double.parseDouble(input.nextLine());
		
		// prompt the user for the width in inches of a box
		System.out.print("width in inches: ");
		double y = Double.parseDouble(input.nextLine());
		
		// prompt the user for the depth in inches of a box
		System.out.print("depth in inches: ");
		double z = Double.parseDouble(input.nextLine());
		
		if (x <= 0) {
			System.out.println("length cannot be equal to or less than 0, please enter again."); // Checks to see if length is 0 or less
		} else if (y <= 0) {
			System.out.println("width cannot be equal to or less than 0, please enter again."); // Checks to see if width is 0 or less
		} else if (z <= 0) {
			System.out.println("depth cannot be equal to or less than 0, please enter again."); // Checks to see if height is 0 or less
		} else {
			System.out.println("the amount of wood (square feet) needed to make the box is" + (((x*y*2)+(x*z*2)+(y*z*2))/144) + "square feet");
		}
		
		input.close(); // Close scanner
		
		// length     width     depth     result
		//   1          2         2       0.1111111111111111
		//   250        346       567     5894.888888888889
		//   0          12        46      length cannot be equal to or less than 0, please enter again.
		//   2.6        1.5       2.8     0.21361111111111108

	}

}
