//Peng Liang,cs201-202201,Feb. 6, 2022,ArithmeticCalculations
package labs.lab1;

public class ArithmeticCalculations {

	public static void main(String[] args) {
		int myAge = 26;
		int fatherAge = 49;
		int birthYear = 1996;
		double height = 66.93;//inches
		
		int agedifference = fatherAge - myAge;//My age subtracted from my father's age
		int doublebirthYear = birthYear * 2;//My birth year multiplied by 2
		double convertToCms = height * 20.54;//Convert my height in inches to cms
		double convertToFeet = height * 0.833333;//"Convert my height in inches to feet
		int convertToIntInches = (int) height;//Convert my height in inches to integer inches
		
		System.out.println("My age subtracted from my father's age is: " + agedifference);
		System.out.println("My birth year multiplied by 2 is: " + doublebirthYear);
		System.out.println("Convert my height in inches to cms: " + convertToCms);
		System.out.println("Convert my height in inches to feet: " + convertToFeet);
		System.out.println("Convert my height in inches to integer inches: " + convertToIntInches);

	}

}
