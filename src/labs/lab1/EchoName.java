//Peng Liang,cs201-202201,Feb. 6, 2022,EchoName
package labs.lab1;

import java.util.Scanner;

public class EchoName {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
				
		// prompt the user to type something
		System.out.print("Please type your name:");
				
		// read in the next line of text typed by the user
		String name = input.nextLine();
				
		// print out what the user entered
		System.out.println("Echo:" + name);
		
		input.close(); // Close scanner

	}

}
