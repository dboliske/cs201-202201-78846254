//Peng Liang,cs201-202201,Feb. 6, 2022,FirstInitial
package labs.lab1;

import java.util.Scanner;

public class FirstInitial {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// Prompt a user for a first name
		System.out.print("Please type your first name:");
		
		//read in a character
		char c = input.nextLine().charAt(0);
		
		//display the user's first initial to the screen
		System.out.println("first initial:" + c);
		
		input.close(); // Close scanner
		

	}

}
