//Peng Liang,cs201-202201,Feb. 6, 2022,ConvertTemperature
package labs.lab1;

import java.util.Scanner;

public class ConvertTemperature {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompt the user for temperature in Fahrenheit
		System.out.print("Enter the temperature in Fahrenheit: ");
		double x = Double.parseDouble(input.nextLine());
		
		//convert the Fahrenheit to Celsius and display the result
		System.out.println("Convert to Celsius to" + ((x-32)/1.8) + "Celsius");
		
		// prompt the user for temperature in Fahrenheit
		System.out.print("Enter the temperature in Fahrenheit: ");
		double y = Double.parseDouble(input.nextLine());
		
		//convert the Celsius to Fahrenheit and display the result
		System.out.println("Convert to Fahrenheit to" + (32+y*1.8) + "Fahrenheit");
		
		//  Fahrenheit      result1                       Celsius      result2
		//      51          10.555555555555555               78        172.4
		//     -10         -23.333333333333332              -100      -148.0
		//      48.55       9.194444444444443                79.46     175.028
		
		input.close(); // Close scanner

	}

}
