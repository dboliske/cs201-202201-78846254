// Peng Liang,cs201-202201,Apr. 30, 2022,Classroom,sec.#03
package exams.second;

public class Classroom {

    protected String building;  //Instance variable for building
    protected String roomNumber;  //Instance variable for room number
    private int seats;  //Instance variable for seats

    public Classroom() {   // default constructor
    	building = "Turing";
    	roomNumber = "404";
    	seats = 20;
    }
    
    public void setBuilding(String building) {
    	this.building = building;
    }
    
    public void setRoomNumber(String roomNumber) {
    	this.roomNumber = roomNumber;
    }
    
    public void setSeats(int seats) {
    	if (seats > 0) {
    		this.seats = seats;
    	}
    }
    
    // accessors
    public String getBuilding() {
    	return building;
    }
    
    public String getRoomNumber() {
    	return roomNumber;
    }
    
    public int getSeats() {
    	return seats;
    }
    
    @Override
    public String toString() {
    	return " Building: " + building + "; RoomNumber: " + roomNumber + "; Seats: " + seats + ".";
    }
    
}
