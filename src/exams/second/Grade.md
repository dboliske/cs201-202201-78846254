# Final Exam

## Total

88/100

## Break Down

1. Inheritance/Polymorphism:    20/20
    - Superclass:               5/5
    - Subclass:                 5/5
    - Variables:                5/5
    - Methods:                  5/5
2. Abstract Classes:            20/20
    - Superclass:               5/5
    - Subclasses:               5/5
    - Variables:                5/5
    - Methods:                  5/5
3. ArrayLists:                  19/20
    - Compiles:                 5/5
    - ArrayList:                5/5
    - Exits:                    5/5
    - Results:                  4/5
4. Sorting Algorithms:          20/20
    - Compiles:                 5/5
    - Selection Sort:           10/10
    - Results:                  5/5
5. Searching Algorithms:        9/20
    - Compiles:                 5/5
    - Jump Search:              2/10
    - Results:                  2/5

## Comments
1.ok
2.ok
3.Error if the first input is done. -1
4.ok
5.Didn't implememt Jump search algorithm with recursively; -5
  The first while loop condition is wrong. -3
  when inputs are: 1.304, 1.732, 3.142, the program gets result -1 instead of their indexes. -3 
