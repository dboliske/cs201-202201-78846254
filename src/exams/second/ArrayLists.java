// Peng Liang,cs201-202201,Apr. 30, 2022,ArrayLists,sec.#03
package exams.second;

import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		
		ArrayList<Double> numbers = new ArrayList<Double>();
		
		System.out.println("Input numbers and enter 'Done' to find the minimum and the maximum!");
		
		boolean running = false;
		
		do {
			// prompt for numbers
			System.out.print("Input Numbers: ");
			String in = input.nextLine();
			if (in.equalsIgnoreCase("done")) {
                running = true;
		} else {
			try {
				double number = Double.parseDouble(in);
				numbers.add(number);
			} catch (Exception e) {
				System.out.println("Invalid input!");
			}
		}

	    } while (!running);
		
		double max = numbers.get(0);
		double min = numbers.get(0);
		
		if (numbers.size() != 0) {  // find the minimum and the maximum
			for (int i = 0; i < numbers.size(); i++) {
				
				if (numbers.get(i) > max) {	
					max = numbers.get(i);
				}
				
				if (numbers.get(i) < min) {					
					min = numbers.get(i);
				}
			}
			System.out.println("Maximum: " + max);
            System.out.println("Minimum: " + min);

		} else {		
			System.out.println("You haven't input any numbers.");
			}
		input.close();
		}
	}
