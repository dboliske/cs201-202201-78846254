// Peng Liang,cs201-202201,Apr. 30, 2022,ComputerLab,sec.#03
package exams.second;

public class ComputerLab extends Classroom{
	
	// Instance variable
	private boolean computers;
	
	public ComputerLab() {  //default constructor
		
		super();
		computers = true;
	}
	
	
	// mutator method
	public void setComputers(boolean computers) {
		this.computers = computers;
	}
	
	// accessor method
	public boolean hasComputers() {
		return this.computers;
	}
	
	@Override
	public String toString() {
		
		return super.toString() + "Computers: " + computers;
	}

}
