// Peng Liang,cs201-202201,Apr. 30, 2022,Polygon,sec.#03
package exams.second;

public abstract class Polygon {
	
	// Instance variable
	protected String name;
	
	public Polygon() {  //default constructor
		name = "squre";
	}
	
	// mutator
	public void setName(String name) {
		this.name = name;
	}
	
	// accessor
	public String getName() {
		return name;
	}
	
    @Override
    public String toString() {
        return "Name: " + name;
    }

    // abstract method
    public abstract double area();
    public abstract double perimeter();

}
