// Peng Liang,cs201-202201,Apr. 30, 2022,Rectangle,sec.#03
package exams.second;

public class Rectangle extends Polygon{

	// Instance variable
	private double width;
	private double height;
	
	public Rectangle() {  // default constructor
		setName("unamed");
		width = 1.0;
		height = 1.0;
	}
	
	// accessor
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
		
	// mutator
	public void setWidth(double width) {
		
		if (width > 0) {
			this.width = width;
		}
        else {		
			this.width = 1.0;
		}
	}
	
	public void setHeight(double height) {
		
		if (height > 0) {
			this.height = height;
		}
        else {		
			this.height = 1.0;
		}
	}
	
    @Override
    public String toString() {
        return super.toString() + ": width is " + width + "; " + " height is " + height;
    }
    
    // area
    @Override
    public double area() {
        return height * width;
    }
    
    // perimeter
    @Override
    public double perimeter() {
        return 2 * (height + width);
    }
}
