// Peng Liang,cs201-202201,Apr. 30, 2022,Searching,sec.#03
package exams.second;

import java.util.Scanner;

public class Searching {

	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		System.out.print("Search the number: ");
		double in = input.nextDouble();		
		double index = JumpSearch(numbers, in);
		
		// print -1 when the value is not present
		if(index == -1) {  
			System.out.println("-1");
		} else {
			// print the position of where that value is found
			System.out.println(in + " is in the " + (int)index + "th place. ");
		}

		input.close();
	}
	
	// the Jump Search Algorithm
	public static double JumpSearch(double[] numbers, double in) {
		
		double n = (int)Math.sqrt(numbers.length);
		double prev = 0;
		
		while(numbers[(int)Math.min(n, numbers.length - 1)] < in) {
			prev = n;
			n += (int)Math.sqrt(numbers.length);
			if (prev >= numbers.length) {
				return -1;
			}
		}
		
		while (numbers[(int)prev] < in) {
			prev ++;
			if (prev == Math.min(n, numbers.length - 1)) {
				return -1;
			}
		}
			
		if (numbers[(int)prev] == in) {
			return prev;
		}
		return -1;
	}

}
