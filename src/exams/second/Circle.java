// Peng Liang,cs201-202201,Apr. 30, 2022,Circle,sec.#03
package exams.second;

public class Circle extends Polygon{

	// instance variable
    private double radius;
    
    public Circle() {   // default constructor
    	super();
    	radius = 1.0;
    }
    
    // mutator
    public void setRadius(double radius) {
    	if (radius > 0) {
    		this.radius = radius;
    	}
    	else {
    		this.radius = 1.0;
    	}
    }
    
    // accessor
    public double getRadius() {
    	return radius;
    }
    
    @Override
    public String toString() {
        return super.toString() + ": radius is " + radius;
    }
    
    @Override
    public double area() {
        return Math.PI * radius * radius;
    }
    
    @Override
    public double perimeter() {
        return 2.0 * Math.PI * radius ;
    }

}
