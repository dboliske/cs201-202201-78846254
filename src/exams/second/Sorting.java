// Peng Liang,cs201-202201,Apr. 30, 2022,Sorting,sec.#03
package exams.second;

public class Sorting {

	public static void main(String[] args) {
		String[] words = {"speaker", "poem", "passenger", "tale", "reflection", "leader", "quality", "percentage", "height", "wealth", "resource", "lake", "importance"};
		words = sort(words);
		
		for(int i = 0; i < words.length - 1; i++) {
			System.out.print(words[i] + " ");
		}
		System.out.print(words[words.length - 1]);

	}
	
	// selection sort
	public static String[] sort(String[] words) {
		for(int i = 0; i < words.length - 1; i++) {
			int min = i;
			
			for(int j = i + 1; j < words.length; j++) {
				if(words[j].compareTo(words[min]) < 0) {
					min = j;
				}
			}
			
			if(min != i) {
				String temp = words[i];
				words[i] = words[min];
				words[min] = temp;
			}
		}
		return words;
	}

}
