//Peng Liang,cs201-202201,Feb. 25, 2022,Pet,sec.#03
package exams.first;

public class Pet {
	private String name;
	private int age;
	
	public Pet() {
		name = "unnamed";
		age = 0;
	}
	
	public Pet(String name,int age) {
		this.name = name;
		this.age = 0;
		
		setAge(age);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if (age >= 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Object obj) {
		if (!(obj instanceof Pet)) {
			return false;
		}else {
		Pet x = (Pet)obj;
		return this.name.equals(x.getName()) && this.age == x.getAge();
		}
	}
	
	public String toString() {
		return "The pet's name is " + name + " and the age of the pet is " + age;
	}

}
