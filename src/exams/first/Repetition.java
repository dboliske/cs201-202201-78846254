//Peng Liang,cs201-202201,Feb. 25, 2022,Repetition,sec.#03
package exams.first;

import java.util.Scanner;

public class Repetition {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompts the user for an integer
		System.out.print("Enter an integer: ");
		int x = Integer.parseInt(input.nextLine());
		
		for (int i=0;i<x;i++) {
			for (int j=0;j<i;j++) {
				System.out.print("  ");
			}
			for (int k=x-i;k>0;k--) {
				System.out.print("* ");
			}
			System.out.println();
		}
		
		input.close();
	}
}
