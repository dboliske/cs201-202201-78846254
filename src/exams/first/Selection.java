//Peng Liang,cs201-202201,Feb. 25, 2022,Selection,sec.#03
package exams.first;

import java.util.Scanner;

public class Selection {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompts the user for an integer
		System.out.print("Enter an integer: ");
		int x = Integer.parseInt(input.nextLine());
		
		if (x%2 == 0 && x%3 != 0) {
			System.out.println("foo");
		} else if(x%3 == 0 && x%2 != 0) {
			System.out.println("bar");
		} else if(x%2 == 0 && x%3 == 0) {
			System.out.println("foobar");
		} 
		
		input.close();
	}

}
