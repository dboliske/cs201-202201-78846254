//Peng Liang,cs201-202201,Feb. 25, 2022,DataTypes,sec.#03
package exams.first;

import java.util.Scanner;

public class DataTypes {

	public static void main(String[] args) {
		// Create a variable for reading in user input
		Scanner input = new Scanner(System.in);
		
		// prompts the user for an integer
		System.out.print("Enter an integer: ");
		int number = Integer.parseInt(input.nextLine());
		
		// add 65 to the number
		int x = number + 65;
		
		// print the character
		System.out.println("The character is: " + (char)(x));
		
		input.close();
		
	}

}
