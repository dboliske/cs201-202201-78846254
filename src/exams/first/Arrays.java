//Peng Liang,cs201-202201,Feb. 25, 2022,Arrays,sec.#03
package exams.first;

import java.util.Scanner;

public class Arrays {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); //create scanner for user input
		
		String[] arrays = new String[5];
		
		// save into array
		for (int i=0;i<arrays.length;i++) {
			System.out.print("Enter a word(you can only enter 5 words): ");
			arrays[i] = input.nextLine();
		}
		
		// compare one by one
		for (int i=0;i<arrays.length;i++) {
			for (int j=i+1;j<arrays.length;j++) {
				if (arrays[i].equals(arrays[j])) {
					System.out.println(arrays[i] + " appears more than once");
				}
			}
		}
		
		input.close();
	}
}
