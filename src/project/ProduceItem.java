// Peng Liang,cs201-202201,Apr. 28, 2022,ProduceItem,sec.#03
// This class is a subclass of Item, contains the expiration date of the item.
package project;

import java.util.regex.Pattern;

public class ProduceItem extends Item{
	    private String expirationDate;

	    private static String expirationDatePattern = "([1-9]|[0][1-9]|[1][0-2])(/)"  //Regex pattern for the date

	    		 + "([1-9]|[0][1-9]|[12][0-9]|[3][01])(/)" + "([0-9]+)";
	    
	    public ProduceItem() {    	    // default constructor
	        super();
	        this.expirationDate = new String();
	    }
	    
	    // non-default constructor
	    public ProduceItem(String name, double price, String expirationDate) {
	        super(name, price);
	        this.expirationDate = new String();
	        setExpirationDate(expirationDate);
	    }
	    
	    public String getExpirationDate() {  	    // accessor method
	        return expirationDate;
	    }

	    // mutator method
	    public void setExpirationDate(String expirationDate) {
			 if (Pattern.matches(expirationDatePattern, expirationDate)) {

					this.expirationDate = expirationDate;

				 }
					  else { 
						  this.expirationDate = new String();
					  }
	    }
	    
	    // convent to String
	    @Override
	    public String toString() {
	        return super.toString() + "," + expirationDate;
	    }
	    
	    // compare the instance
	    @Override
	    public boolean equals(Object obj) {
	        if (!(obj instanceof ProduceItem)){
	            return false;
	        }
	        ProduceItem produceItem = (ProduceItem) obj;
	        return super.equals(obj) 
	        		&& this.expirationDate.equals(produceItem.getExpirationDate());
	    }
	    
	    @Override
	    public String toCSV() {
	        return "ProduceItem," + super.toCSV() + "," + expirationDate;
	    }
}