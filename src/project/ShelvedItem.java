// Peng Liang,cs201-202201,Apr. 28, 2022,ShelvedItem,sec.#03
// This class is a subclass of Item, contains the shelved item.
package project;

public class ShelvedItem extends Item {
	public ShelvedItem() {      // default constructor
        super();
    }
	
    // non-default constructor
    public ShelvedItem(String name, double price) {
        super(name, price);
    }
    
    @Override
    public boolean equals(Object obj) {   // compare the instance
        if (!(obj instanceof ShelvedItem)) {
            return false;
        }
        return super.equals(obj);
    }

}
