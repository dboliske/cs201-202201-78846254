// Peng Liang,cs201-202201,Apr. 28, 2022,AgeRestrictedItem,sec.#03
// This class is a subclass of Item, contains the age restricted item.
package project;

public class AgeRestrictedItem extends Item{
	private int age;
	
	public AgeRestrictedItem() {  	// default constructor
		super();
		this.age = 18;
	}
	
	// non-default constructor
    public AgeRestrictedItem(String name, double price, int age) {
        super(name, price);
        this.age = 18;
        this.setAge(age);
    }
    
    public int getAge() {    // accessor method
        return age;
    }
    
    public void setAge(int age) {      // accessor method
        if (age >= 0) {
            this.age = age;
        }
    }
    
    // convent to String
    @Override()
    public String toString() {
        return super.toString() + " The age restriction is " + age + ".";
    }
    
    // compare the instance
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AgeRestrictedItem)) {
            return false;
        }
        AgeRestrictedItem a = (AgeRestrictedItem) obj;
        return super.equals(obj) && this.age == a.getAge();
    }
    
    @Override
    public String toCSV() {
        return super.toCSV() + "," + age;
    }
}
