// Peng Liang,cs201-202201,Apr. 28, 2022,Item,sec.#03
// This class contains all the information of the general class
package project;

public class Item {

	private String name;
	private double price;
	
    // default constructor	
	public Item() {
		this.name = "unamed";
		this.price = 0.0;
	}

    // non-default constructor
    public Item (String name, double price) {
        this();
        setName(name);
        setPrice(price);
    }
	
    // accessor method of name
    public String getName() {
        return name;
    }
    
    // mutator method of name
    public void setName(String name) {
        this.name = name;
    }
    
    // accessor method of price
    public double getPrice() {
        return price;
    }

    // mutator method of price
    public void setPrice(double price) {
        if (price >= 0) {
            this.price = price;
        }
    }
    
    // convert to String
    public String toString() {
        return " The price of " + name + " is " + " $" + price + ".";
    }
    
    // compare the instance
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Item)) {
            return false;
        }
        
        Item item = (Item) obj;
        return this.name.equals(item.getName()) && this.price ==  item.getPrice();
    }
    
    public String toCSV() {
        return name + "," + price;
    }
    
}
